package com.guruofjava.hibernateapp;

import com.guruofjava.hibernateapp.model.Contact;
import com.guruofjava.hibernateapp.model.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class HibernateApp {

    static EntityManagerFactory emf = null;
    static EntityManager em = null;

    public static void listUsers() {
        em = emf.createEntityManager();

//        List<User> userList = em.createQuery("FROM User e", User.class)
//                .getResultList();
        List<User> userList = em.createNamedQuery("User.findAll", User.class)
                .getResultList();

        userList.forEach(u -> {
            System.out.format("# %3d # %-20s # %-30s #%n", u.getUserId(),
                    u.getName(), u.getEmail());
        });

        em.close();
    }

    public static void printUserByEmail(String email) {
        em = emf.createEntityManager();
//        User u = em.createQuery("SELECT e FROM User e WHERE e.userId=" + userId, User.class)
//                .getSingleResult();

        User u = em.createNamedQuery("User.findByEmail", User.class)
                .setParameter("emailId", email)
                .getSingleResult();

        System.out.println(u);

        em.close();
    }

    public static void printUser(Integer userId) {
        em = emf.createEntityManager();
//        User u = em.createQuery("SELECT e FROM User e WHERE e.userId=" + userId, User.class)
//                .getSingleResult();

        User u = em.createNamedQuery("User.findById", User.class)
                .setParameter(1, userId)
                .getSingleResult();

        System.out.println(u);

        em.close();
    }

    public static void addUser() {
        em = emf.createEntityManager();

        User temp = new User(0, "Michael", "michael@mail.com", "");
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //em.persist(temp);
        temp = em.merge(temp);
        temp.setEmail(temp.getEmail().toUpperCase());

        tx.commit();
        em.close();
    }

    public static void deleteUser(Integer userId) {
        em = emf.createEntityManager();
        em.getTransaction().begin();

        User u = em.createQuery("SELECT e FROM User e WHERE e.userId=" + userId, User.class)
                .getSingleResult();

        em.remove(u);

        System.out.println(u);

        em.getTransaction().commit();
        em.close();

    }

    public static void printUserWithContactsEnhanced(Integer userId) {
        em = emf.createEntityManager();
        User u = em.createQuery("SELECT e FROM User e WHERE e.userId=" + userId, User.class)
                .getSingleResult();

        System.out.println(u);

        u.getContactList().forEach(c -> {
            System.out.format("# %3d # %-20s # %-30s # %-15s #%n",
                    c.getContactId(), c.getName(), c.getEmail(), c.getMobile());
        });

        em.close();
    }

    public static void printUserWithContacts(Integer userId) {
        em = emf.createEntityManager();
        User u = em.createQuery("SELECT e FROM User e WHERE e.userId=" + userId, User.class)
                .getSingleResult();

        System.out.println(u);

        List<Contact> userContacts = em
                .createQuery("SELECT c FROM Contact c WHERE c.userId=" + userId, Contact.class)
                .getResultList();

        userContacts.forEach(c -> {
            System.out.format("# %3d # %-20s # %-30s # %-15s #%n",
                    c.getContactId(), c.getName(), c.getEmail(), c.getMobile());
        });

        em.close();
    }

    public static void printContact(Integer contactId) {
        em = emf.createEntityManager();
        Contact c = em.createQuery("SELECT c FROM Contact c WHERE c.contactId=" + contactId,
                Contact.class).getSingleResult();

        System.out.format("# %3d # %-20s # %-30s # %-20s #%n",
                c.getContactId(), c.getName(), c.getEmail(), c.getUser().getName());

        em.close();
    }

    public static void main(String[] args) {
        emf = Persistence.createEntityManagerFactory("com.guruofjava.hibernate");

        //addUser();
        //listUsers();
//        deleteUser(39);
//        listUsers();
        //printUserWithContactsEnhanced(1);
        //printContact(6);
        //listUsers();
        printUserByEmail("danny@mail.com");
        //deleteUser(3);

        emf.close();
    }
}
